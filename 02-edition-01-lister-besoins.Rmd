# (PART) Équipe éditoriale {.unnumbered}

# [edition] Comment définir ses besoins et le contenu {#lister-besoins}

Les membres du comité éditorial ([EdCmd], [Red], [Rel]) vont devoir définir le contenu du document.
Dans tous les cas, il s'agit de trouver une manière de définir le contenu de telle sorte que les [Dev] puissent intégrer au mieux les besoins du comité.  

## Écrire des "User stories" et proposer une trame de publication

> "Dîtes-moi ce que vous cherchez à faire, c'est moi, le développeur, qui vous dirai comment nous allons le faire."

Dans un premier temps, ne cherchez pas à savoir ce qui est possible ou pas techniquement. 
Ne listez pas non plus ce que vous croyez être la bonne manière de faire les choses.
Commencez par **définir à quelles questions le document doit répondre**. 
Vous pouvez écrire des phrases précises comme :

- *"J'aimerais montrer le nombre de logements vacants dans ma région pour l'année dernière."*
- *"J'aimerais qu'il soit possible de comparer ce nombre aux cinq années précédentes en un coup d'oeil, sur un graphique par exemple."* 
Laisser le soin aux [Dev] / _data analysts_ de faire les propositions de représentation.
- *"J'aimerais conserver l'évolution en nombre et en pourcentage dans une table à part, pour décider moi-même si je commente ces chiffres dans une analyse détaillée."* 
Laisser le soin aux [Dev] / _data analysts_ de faire les propositions de présentation / stockage des données.

Une fois la liste des besoins définie, les rédacteurs [Red]) peuvent **organiser le contenu en thématiques** et **proposer une trame** écrite des chapitres de la publication PROPRE.
Chaque chapitre contiendra des sections nommées, des demandes de chiffres clés mis en avant, des demandes de tables et graphiques, du texte de présentation.
Gardez toujours en tête les messages principaux, communs, que vous souhaitez faire passer.
Pensez aux lecteurs et lectrices des différentes déclinaisons de vos rapports (année, zone géographique, ...) : peuvent-ils retrouver les mêmes informations, de la même manière, à chaque fois ?  

## Quels outils utiliser pour partager vos besoins avec les développeurs ?

Vous l'avez compris, la communication est la clé. 
Maintenant, vous avez besoin de savoir comment vous allez partager votre liste de besoins avec les développeurs.
Dans la section ["Les outils de travail"](#coordonner-outils), nous avons parlé d'Etherpad, Gitlab et Rocket Chat.
Voici le premier endroit où ils entrent en jeu.
Libre à vous d'utiliser des outils alternatifs.

### Etherpad : prendre des notes collaboratives pendant les réunions

Avec un outil comme Etherpad, vous pouvez travailler sur un document collaboratif pendant vos réunions. 
Voyez avec votre employeur si vous avez ce genre d'outil accessible en interne.
Le format d'écriture est du texte brut.
Selon le pad collaboratif que vous utilisez, vous pouvez éventuellement formater le texte avec un menu en clic bouton ou écrire directement avec la syntaxe markdown. 
Pendant la réunion, **chacun est encouragé à participer à la rédaction** et la correction du compte-rendu et de la trame de publication.
À la fin de votre réunion, vous pouvez télécharger le document au format markdown.

### _git_ : Versionner la trame de publication

Il est **recommandé aux éditeurs ([Red], [Rel]) de se former à _git_** pour gérer au mieux l'évolution des versions de la trame de publication. 
Souvenez-vous, nous essayons de faire en sorte qu'éditeurs et développeurs soient liés par un projet commun, avec des outils communs. 
git est aussi l'outil que les développeurs vont utiliser pour le développement.  
Pourquoi utiliser git pour votre trame ?

- La trame et la liste des besoins que vous aller fournir sera découpée par les développeurs en plusieurs _tickets_, des sous-parties à développer.  
Vous allez sûrement apporter des modifications du document en cours de route, en ajoutant de nouvelles sections par exemple.
Comment les développeurs pourront-ils être au courant et créer les nouveaux tickets en étant sûrs de n'avoir rien oublié ? 
L'utilisation de _git_, un logiciel de versionnement, leur permettra de savoir ce qui a été changé depuis la dernière fois qu'ils sont allés récupérer les besoins.
- Ensuite, après votre réunion commune, vous allez repasser chacun, un par un, sur la trame rédigée pendant la réunion, pour apporter vos propositions et précisions, à tête reposée. 
Comment allez-vous vous assurer que la version envoyée à l'éditeur #3 est bien la version corrigée par l'éditrice #2 ? 
Comment vous faîtes si la personne précédente a oublié de mettre le mode "édition / correction" en marche ?
Où allez-vous stocker toutes les versions passées et les informations sur les choix que vous avez opérés ?
_git_ est là pour ça. 
Il est fait pour **travailler de manière collaborative, en parallèle, en asynchrone**, en conservant et centralisant toutes les modifications.
Et si jamais deux éditeurs travaillent en même temps, _git_ saura vous proposer de fusionner les versions de l'un et de l'autre.
- Enfin, comme indiqué, _git_ est aussi l'outil des développeurs.
S'il doit y avoir une formation à cet outil, vous pouvez demander à ce qu'elle soit spécifique à git et à markdown, mais pas à un langage de programmation.
De cette manière, elle sera utile à la fois aux éditeurs et aux développeurs.

Le stockage de la trame de publication peut se réaliser dans le projet de développement en cours (recommandé) ou bien dans un projet séparé. Avec la trame de publication dans un dossier du package R, nommé `"trame/"`, vous avez la possibilité de demander aux développeurs de vous inclure dans le projet. Vous vous concentrerez uniquement sur le sous-dossier qui vous concerne. Laisser le reste aux développeurs. L'autre possibilité est de créer un projet GitLab à part. Celui-ci peut réduire la charge mentale de l'équipe d'édition, mais augmente la charge de travail de l'équipe de développement. Voir ["Rédiger et fournir les textes en RMarkdown"](#rediger-textes) pour plus de détails sur votre participation à la rédaction.

### GitLab Wiki : Stocker les comptes-rendu de réunions et la trame de publication

Puisque vous avez pris vos notes au format markdown, vous pouvez les insérer en l'état dans le Wiki de Gitlab. 
C'est un peu l'**alternative à l'utilisation directe de _git_** qui nécessite toutefois une bonne organisation et une communication accrue avec les développeurs.  

Pour rappel, nous faisons en sorte de gérer le projet au maximum au même endroit. 
D'où l'utilisation de Gitlab, que vous retrouverez de nouveau pour les éditeurs dans la partie ["Suivre le traitement de ses besoins"](#suivi-besoins) et pour les [Dev] dans toutes les parties qui leur sont dédiées...  

- La première page du Wiki (Home) est utilisée pour lister les liens vers les autres pages.
- Créez **une page pour stocker tous les comptes-rendus de réunions**. 
- Créez **une page pour stocker la trame de publication**. 

La page du Wiki avec **la trame de publication sera utilisée par les [ChfDev]** pour lister les numéros de tickets ouverts, sur le même projet Gitlab.
Ceci permettra aux [Red] / [Rel] de suivre le traitement de leurs demandes (on en parle un peu plus dans ["Suivre le traitement de ses besoins"](#suivi-besoins)).
Toutefois, si les [ChfDev] sont passés une fois sur cette page, il n'y a pas forcément de raison qu'ils y reviennent.
C'est là qu'intervient le logiciel de Chat.
Dans la chaîne générale, **signalez aux [ChfDev] toute modification** apportée au Wiki après la première livraison. 
N'hésitez pas à donner des détails.
Mieux vaut trop d'informations que pas assez. 
> Attention, si les [ChfDev] sont déjà passés sur le Wiki et qu'il y a déjà un ticket ouvert pour la ligne que vous souhaitez modifier, alors c'est trop tard. 
Il faut apporter les nouvelles informations dans le ticket correspondant. 
On vous explique comment dans ["Suivre le traitement de ses besoins"](#suivi-besoins)

_Note: Dans les options de GitLab, il est possible de ne rendre le wiki accessible qu'aux membres du projet. Sinon, dans le cas d'un projet open-source, tout est lisible, que les utilisateurs soient enregistrés sur GitLab ou non._

### Rocket Chat : communiquer de manière asynchrone entre éditeurs et avec les développeurs

Le logiciel de **Chat est un lien indispensable** au sein du projet (Rocket Chat, Slack, Glip, ...). 
C'est une communication moins formelle que les mails qui permet une meilleure réactivité.
Toutes les informations sont stockées au même endroit, et organisées par catégories et informations. 
Nous n'allons pas faire un guide d'utilisation d'un logiciel de Chat, mais il est recommandé d'**avoir recourt aux "Fils de discussion"** (*Thread*) pour éviter de surcharger la chaîne principale avec des informations qui ne concernent qu'un nombre limité de personnes.
Il est aussi recommandé de **réagir aux différentes informations** qui passent. 
Vous verrez qu'il est possible de mettre une réaction avec un emoji. 
Au minimum cela informe que l'information a été lue.
Cela rend les échanges plus vivants.
On ne peut pas voir les visages des gens lorsqu'ils écrivent ou lisent, les emojis permettent, comme leur nom l'indique, de faire passer les émotions.
N'hésitez pas à **utiliser les emojis pour partager votre joie**, l'accomplissement d'une tâche du projet, ... `r emo::ji("tada")`



