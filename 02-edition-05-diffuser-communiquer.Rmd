# [edition] Produire, diffuser et communiquer{#diffuser-communiquer}

Lorsque le projet de développement est totalement validé, il est temps de l'utiliser pour produire.
Souvenons-nous que nous sommes dans un "PROcessus de Publications REproductibles" (PROPRE), nous avons donc des publications à produire.
La production de vos documents statiques se réalise avec le package qui vient d'être développé. 
Il s'agit maintenant de l'utiliser pour effectivement produire les différents documents reproductibles. 
Selon votre thématique, vous aurez des documents séparés selon les périodes et les zones géographiques que vous aurez définies.
Chaque [Red] devra produire son document réproductible, si nécessaire y ajouter ses parties personnalisées, puis renvoyer sa production à l'équipe du projet.
Dans les meilleures conditions, vous aurez un espace web dédié à la publication de vos documents.

> Communiquez sur votre projet, c'est un gros travail, vous avez bien travaillé, soyez-en fiers !

_Pensez à noter une phrase qui indique que le présent document a été créé avec R et Markdown grâce un "PROcessus de Publications REproductibles" (PROPRE), documenté, testé, versionné avec une forge logicielle adaptée._
